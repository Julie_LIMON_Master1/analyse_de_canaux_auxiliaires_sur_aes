# Analyse de canaux auxiliaires sur AES

En collaboration avec CLEMENT Christelle, GBEDJI Sedami, PLUMEL Laurianne.

Nous avons développé une partie d'une attaque sur le premier tour de l'AES. 
L'objectif était de retrouver le message en clair à partir du message chiffré, et de l'analyse de la consommation de courant en sortie de la fonction SubBytes du premier tour

Pour plus de détail, se référer aux rapports.
